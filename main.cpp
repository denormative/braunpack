#include <iostream>
#include <fstream>
#include <string>
#include <sys/stat.h>

#include "tinfl.c"

using namespace std;

const bool print_debug = false;

void doFile(ifstream &infile, std::string dirName) {
	cout << dirName << endl;

	if (!infile.is_open())
		return;

	cout << "File opened..." << endl;

	char temp4[4];
	infile.read (temp4, 4);

	if (print_debug) cout << temp4 << endl;
	if (strcmp(temp4, "PDA") != 0)
		return;

	cout << "Header string ok..." << endl;

	int version;
	infile.read ((char*)&version, 4);
	cout << "Version: " << version << endl;

	int tableOffset;
	infile.read ((char*)&tableOffset, 4);
	if (print_debug) cout << "Table Offset: " << tableOffset << endl;

	int numFiles;
	infile.read ((char*)&numFiles, 4);
	cout << "#Files: " << numFiles << endl;

	infile.seekg(tableOffset, ios_base::beg);

#ifdef __APPLE__
	mkdir(dirName.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
#else
	mkdir(dirName.c_str());
#endif

	for (int i=0; i<numFiles; i++) {
		if (print_debug) cout << i << " " << numFiles << endl;
		long long dateTime;
		infile.read ((char*)&dateTime, 8);
		if (print_debug) cout << "Timestamp: " << dateTime << endl;
		int zSize;
		infile.read ((char*)&zSize, 4);
		if (print_debug) cout << "Compressed Size: " << zSize << endl;
		int size;
		infile.read ((char*)&size, 4);
		if (print_debug) cout << "Normal Size: " << size << endl;
		short nsize;
		infile.read ((char*)&nsize, 2);
		if (print_debug) cout << "Name Size: " << nsize << endl;
		short unk;
		infile.read ((char*)&unk, 2);
		if (print_debug) cout << "Unknown: " << unk << endl;
		int offset;
		infile.read ((char*)&offset, 4);
		if (print_debug) cout << "Offset: " << offset << endl;
		char *name = new char[nsize+1];
		infile.read (name, nsize);
		name[nsize]='\0';
		if (print_debug) cout << "Name: " << name << endl;

		unsigned long cmp_len = zSize;
		unsigned long uncomp_len = size;

		ifstream::pos_type tempPos = infile.tellg();
		infile.seekg(offset, ios_base::beg);
		unsigned char *pCmp = new unsigned char[cmp_len];
		infile.read((char*)pCmp, cmp_len);
		infile.seekg(tempPos, ios_base::beg);

		string newFileName = dirName + "/" + name;

		if (size+16==zSize) {
			ofstream newFile(newFileName.c_str(), ios::out | ios::binary);
			newFile.write((char*)(pCmp+16), cmp_len-16);
			newFile.close();
		}
		else {
			unsigned char *pUncomp = new unsigned char[uncomp_len];

			size_t cmp_status = tinfl_decompress_mem_to_mem(pUncomp, uncomp_len, pCmp+16, cmp_len, 0);
			if (print_debug) cout << "CMP Status: " << cmp_status << endl;
			if (print_debug) cout << "Uncomp Len: " << uncomp_len << endl;

			ofstream newFile(newFileName.c_str(), ios::out | ios::binary);
			newFile.write((char*)pUncomp, uncomp_len);
			newFile.close();

			delete pUncomp;
		}
		cout << newFileName << endl;

		delete pCmp;
		delete name;

		if (print_debug) cout << i << " " << numFiles << endl;

	}
}

void doFiles() {
	ifstream s00bra("S00.bra", ios::in | ios::binary);
	ifstream s01bra("S01.bra", ios::in | ios::binary);
	ifstream s02bra("S02.bra", ios::in | ios::binary);
	ifstream s03bra("S03.bra", ios::in | ios::binary);

	doFile(s00bra, "S00");
	doFile(s01bra, "S01");
	doFile(s02bra, "S02");
	doFile(s03bra, "S03");
}

int main()
{
	cout << "Starting..." << endl;


	doFiles();

	cout << "Finished." << endl;
	return 0;
}

