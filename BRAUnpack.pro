TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

QMAKE_MAC_SDK = macosx10.9

SOURCES += main.cpp tinfl.c

win32 {
	QMAKE_LFLAGS = -static -static-libgcc
}
